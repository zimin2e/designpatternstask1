﻿using System;

namespace DataLayer
{
    public class Tank2D : IMovable<int[],int>, IRotatable<int[],int>, IShootable<int[],int>
    {
        public int[] Location { get; set; } = new int[2];
        public int[] Velocity { get; set; } = new int[2];


        public void Move( int time) 
        {
            if (Velocity == null || Velocity.Length != 2) throw new ArgumentException();
            if (Location == null || Location.Length != 2) throw new ArgumentException();

            Location[0] += Velocity[0] * time;
            Location[1] += Velocity[1] * time;
        }

        public void Rotate(int[] point, int degree ) 
        {
            if (point == null || point.Length != 2) throw new ArgumentException();
            if (Location == null || Location.Length != 2) throw new ArgumentException();

            Location[0] = (int)(point[0] * Math.Cos(degree/Math.PI)) + (int)(point[1] * Math.Sin(degree / Math.PI));
            Location[1] = (int)(point[0] * Math.Sin(degree / Math.PI)) + (int)(point[0] * Math.Cos(degree / Math.PI));
        }


        public int[] gunVector { get; set; }

        public void GunRotate( int degree) 
        {
            if (gunVector == null || gunVector.Length != 2) throw new ArgumentException();
            if (Location == null || Location.Length != 2) throw new ArgumentException();

            gunVector[0] = (int)(gunVector[0] * Math.Cos(degree / Math.PI)) + (int)(gunVector[1] * Math.Sin(degree / Math.PI));
            gunVector[1] = (int)(gunVector[0] * Math.Sin(degree / Math.PI)) + (int)(gunVector[0] * Math.Cos(degree / Math.PI));
        }
    }

}

﻿namespace DataLayer
{
    interface IRotatable<T,U>
    {
        void Rotate(T point, U degree);
    }

}

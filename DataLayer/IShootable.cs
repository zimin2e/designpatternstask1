﻿namespace DataLayer
{
    interface IShootable<T,U>
    {
        T gunVector { get; set; }
        void GunRotate( U degree);
    }

}

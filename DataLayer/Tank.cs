﻿using System.Collections.Generic;
using System.Linq;

namespace DataLayer
{
    interface IMovable<T, U> 
    {
        T Location { get; set; }
        T Velocity { get; set; }
        void Move(U time);
    }

}

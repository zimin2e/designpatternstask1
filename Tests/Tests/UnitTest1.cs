using System;
using Xunit;
using DataLayer;

namespace Tests
{
    public class UnitTest1
    {

        /*
          
    ���������� ����: ��� �������, ������������ � ����� (12, 5) � ����������� �� ��������� (-7, 3) �������� ������ ��������� ������� �� (5, 8) - 1 ����

    ���������� ����: ������� �������� ������, � �������� ���������� ��������� ��������� � ������������, �������� � ������ - 1 ����

    ���������� ����: ������� �������� ������, � �������� ���������� ��������� �������� ���������� ��������, �������� � ������ - 1����

    ���������� ����: ������� �������� ������, � �������� ���������� �������� ��������� � ������������, �������� � ������ - 1 ����


          
         */

        /*
         ��� �������, ������������ � ����� (12, 5) � ����������� �� ��������� (-7, 3) �������� ������ ��������� ������� �� (5, 8)
         */
        [Fact]
        public void TestSetLocation()
        {
            Tank2D tank = new Tank2D();
            tank.Location[0] = 12;
            tank.Location[1] = 5;

            tank.Velocity = new int[] { -7, 3 };

            int time = 1; //c

            tank.Move(1);
            Assert.True(tank.Location[0] == 5);
            Assert.True(tank.Location[1] == 8);
        }

        /*
         ������� �������� ������, � �������� ���������� �������� ��������� � ������������, �������� � ������
         */
        [Fact]
        public void Test_Location_outOfRange()
        {
            Tank2D tank = new Tank2D();

            tank.Location = null;
            tank.Velocity = new int[] { -7, 3 };

            Assert.Throws<ArgumentException>(
                 () => tank.Move(1)
            );
        }


        /*
         ������� �������� ������, � �������� ���������� ��������� �������� ���������� ��������, �������� � 
        */
        [Fact]
        public void Test_Velocity_null()
        {
            Tank2D tank = new Tank2D();

            tank.Location = null;

            Assert.Throws<ArgumentException>(
                 () => tank.Move(1)
            );
        }


        /*
        ������� �������� ������, � �������� ���������� ��������� ��������� � ������������, �������� � ������
        */
        [Fact]
        public void Test_Velocity_outOfRange()
        {
            Tank2D tank = new Tank2D();

            tank.Velocity = null;

            Assert.Throws<ArgumentException>(
                 () => tank.Move( 1)
            );
        }


        /*
        ������� �������� ������, � �������� ���������� �������� ��������� � ������������, �������� � ������
        */
        [Fact]
        public void Test_CanNotChangeLocation()
        {
            Tank2D tank = new Tank2D();
            tank.Location[0] = 12;
            tank.Location[1] = 5;

            tank.Velocity = new int[] { -7, 3 };

            int time = 1; //c

            Assert.Throws<ArgumentException>(
                 () => tank.Rotate(null, 10)
            );
        }

    }
}
